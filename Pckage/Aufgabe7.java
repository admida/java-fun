package Pckage;

public class Aufgabe7 extends MiniJava {
    public static void main(String[] args) {

        int guthaben = 100;

        while (guthaben > 0) {
            // Einsatz durch Eingabe festlegen
            int einsatz = readInt("Wie viel willst du einsetzten?");
            int feld = readInt("Auf welches Feld zwischen 2 und 12?");

            // Guthaben prüfen
            if (einsatz == 0) {
                System.out.println("Vielen Dank fürs Spielen. Du hast " + guthaben + " Euro auszahlen lassen.");
                guthaben = 0;
            } else if (guthaben - einsatz >= 0) {
                // jetzt wird gespielt
                int wuerfel1 = dice();
                int wuerfel2 = dice();
                int wuerfelSum = wuerfel1 + wuerfel2;

                guthaben = guthaben - einsatz;

                if (feld == 7 && wuerfelSum == 7) {
                    guthaben = guthaben + einsatz*3;
                } else if (feld == wuerfelSum && feld != 7) {
                    guthaben = guthaben + einsatz*2;
                } else if ((feld <=6 && wuerfelSum <= 6) || (feld >= 8 && wuerfelSum >= 8)){
                    guthaben = guthaben + einsatz;
                }
                System.out.println("Die " + wuerfelSum + " wurde gewürfelt!");
                System.out.println("Jetzt noch " + guthaben + " Euro");
            } else {
                System.out.println("Nicht genug Geld. Du hast nur noch: " + guthaben);
            }
        }
        System.out.println("Kein Geld mehr.");
    }
}
