package Pckage;

public class midnight extends MiniJava {

    public static void output(double x1, double x2) {
        if (Double.isNaN(x1) && Double.isNaN(x2)) {
            write("Es gibt keine Lösungen.");
        } else if (!Double.isNaN(x1) && !Double.isNaN(x2)) {
            write("Die Lösungen lauten " + x1 + " und " + x2 + ".");
        } else {
            if (Double.isNaN(x1)) {
                write("Die Lösung lautet: " + x2);
            } else {
                write("Die Lösung lautet: " + x1);
            }
        }
    }

    public static void main(String[] args) {
        int a = readInt("Gib mir a: ");
        int b = readInt("Gib mir b: ");
        int c = readInt("Gib mir c: ");

        double x1 = (-b + Math.sqrt(b ^ 2 + 4 * a * c)) / (2 * a);
        double x2 = (-b + Math.sqrt(b ^ 2 - 4 * a * c)) / (2 * a);

        output(x1, x2);
    }

}